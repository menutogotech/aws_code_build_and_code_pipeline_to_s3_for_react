# variables.tf
variable "workspace" {
  default     = "default"
  type        = string
  description = "ENVIRONMENT name for resource naming"
}


variable "service_name" {
  default     = ""
  type        = string
  description = "name service"
}

locals {
  app_name = "${var.service_name}-menutogo-${var.workspace}"
}

variable "repository_name" {
  description = "Name repository in CodeCommit"
  default     = "" # example : svc-api-validate
}


variable "pipeline_role_name" {
  description = "pipeline role execute aws_codepipeline"
  default     = "PipelineExecutionRole"
}



variable "branch" {
  description = "Name branch code_pipeline"
  default     = ""
}

variable "pipeline_bucket" {
  description = "bucket S3 for artifacts pipeline"
  default     = ""
}

variable "deploy_bucket" {
  description = "bucket S3 for deploy build"
  default     = ""
}


variable "app_env_vars" {
  type        = map(string)
  description = "List of environment variables "
  default = {
    "default" = "default"
  }
}
