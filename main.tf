# Roles Init Section
locals {
  pipeline_role_name = "${local.app_name}-${var.pipeline_role_name}"
}


data "aws_iam_policy_document" "assume_by_pipeline" {
  statement {
    sid     = "AllowAssumeByPipeline"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com", "codecommit.amazonaws.com", "codebuild.amazonaws.com"]
    }
  }

}

resource "aws_iam_role" "pipeline_role" {
  name               = local.pipeline_role_name
  assume_role_policy = data.aws_iam_policy_document.assume_by_pipeline.json
  tags = {
    Environment = var.workspace
  }
}

data "aws_iam_policy_document" "pipeline" {
  statement {
    sid    = "AllowS3"
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "AllowCodeBuild"
    effect = "Allow"

    actions = [
      "codebuild:BatchGetBuilds",
      "codebuild:StartBuild",
    ]

    resources = ["${aws_codebuild_project.codebuildRecipe.arn}"]
  }

  statement {
    sid       = ""
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "codecommit:CancelUploadArchive",
      "codecommit:GetBranch",
      "codecommit:GetCommit",
      "codecommit:GetUploadArchiveStatus",
      "codecommit:UploadArchive"
    ]

  }

  statement {
    sid       = ""
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "codedeploy:CreateDeployment",
      "codedeploy:GetApplication",
      "codedeploy:GetApplicationRevision",
      "codedeploy:GetDeployment",
      "codedeploy:GetDeploymentConfig",
      "codedeploy:RegisterApplicationRevision"
    ]
  }

  statement {
    sid       = ""
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ec2:*",
      "elasticloadbalancing:*",
      "autoscaling:*",
      "cloudwatch:*",
      "s3:*",
      "sns:*",
      "cloudformation:*",
      "ecs:*"
    ]

  }

  statement {
    sid       = ""
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "codebuild:BatchGetBuilds",
      "codebuild:StartBuild"
    ]

  }

}

resource "aws_iam_role_policy" "pipeline" {
  role   = aws_iam_role.pipeline_role.name
  policy = data.aws_iam_policy_document.pipeline.json
}


resource "aws_iam_role_policy_attachment" "CloudWatchLogsFullAccess" {
  role       = aws_iam_role.pipeline_role.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}


# Roles End Section
#----------------------------------------------------------------------|

# CodeBuild Init Section
resource "aws_codebuild_project" "codebuildRecipe" {
  name          = "${local.app_name}-recipe"
  build_timeout = "15"
  service_role  = aws_iam_role.pipeline_role.arn

  cache {
    type = "NO_CACHE"
  }

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type    = "BUILD_GENERAL1_SMALL"
    image           = "aws/codebuild/standard:2.0"
    type            = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      name  = "BUILD_REPOSITORY_NAME"
      value = local.app_name
      type  = "PLAINTEXT"
    }

    dynamic "environment_variable" {
      for_each = var.app_env_vars
      content {
        name  = environment_variable.key
        value = environment_variable.value
        type  = "PLAINTEXT"
      }
    }
  }

  source {
    type = "CODEPIPELINE"
  }

  tags = {
    Environment = var.workspace
  }

  depends_on = [aws_iam_role.pipeline_role]
}


# CodeBuild End Section
#----------------------------------------------------------------------|
# CodePipeline Init Section

resource "aws_codepipeline" "codepipelineRecipe" {
  name     = "${local.app_name}-recipe"
  role_arn = aws_iam_role.pipeline_role.arn
  stage {
    name = "Source"

    action {
      name     = "SourceDocker"
      category = "Source"
      owner    = "AWS"
      provider = "CodeCommit"
      version  = "1"

      output_artifacts = ["SourceArtifact"]

      configuration = {
        RepositoryName       = var.repository_name
        BranchName           = var.branch
        PollForSourceChanges = "true"
      }
    }
  }




  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      version          = "1"
      input_artifacts  = ["SourceArtifact"]
      output_artifacts = ["BuildArtifact"]

      configuration = {
        ProjectName   = aws_codebuild_project.codebuildRecipe.name
        PrimarySource = "SourceArtifact"
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "S3"
      input_artifacts = ["BuildArtifact"]
      version         = "1"

      configuration = {
        BucketName = var.deploy_bucket
        Extract    = "true"
        CannedACL  = "public-read"
      }
    }
  }

  artifact_store {
    location = var.pipeline_bucket
    type     = "S3"
  }

  depends_on = [aws_iam_role.pipeline_role, aws_iam_role_policy.pipeline]

}

# CodePipeline End Section
